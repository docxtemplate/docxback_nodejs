const express= require('express');
const morgan = require('morgan');
const docx = require("docx");
const fs= require('fs');
const mammoth = require("mammoth");
const multer = require('multer');
const cors = require('cors');
const docxConverter = require('docx-pdf');
const path = require('path');

const app= express();
const PORT=3000;
const API_KEY = 'gritzzz@inbox.ru_KV5OjOxUbztnq7Ub1ipX332X3iXSY08kh80REx77zaUZJ15xmmQt53S6a7146Lk8';

const uploadDir = 'uploads';
if (!fs.existsSync(uploadDir)){
    fs.mkdirSync(uploadDir);
}

// Настройка multer для сохранения загружаемых файлов
const storage = multer.diskStorage({
    destination: function (req, file, cb) {
        cb(null, 'uploads/') // Убедитесь, что этот каталог существует
    },
    filename: function (req, file, cb) {
        cb(null, file.fieldname + '-' + Date.now() + '.docx')
    }
});
const upload = multer({ storage: storage });
app.use(cors());
app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))

async function extractPlaceholders(filePath) {
    try {
        const result = await mammoth.extractRawText({ path: filePath });
        const text = result.value; // The raw text
        const messages = result.messages;

        // Использование регулярного выражения для поиска конструкций вида {{name}}
        const regex = /{{(.*?)}}/g;
        let match;
        const placeholdersSet = new Set(); // Используйте Set для хранения уникальных placeholders

        while ((match = regex.exec(text)) !== null) {
            placeholdersSet.add(match[1]); // Добавляйте найденные значения в Set
        }

        const placeholders = Array.from(placeholdersSet); // Преобразуйте Set обратно в массив
        return placeholders;
    } catch (err) {
        console.log(err);
        throw new Error('File cannot be processed');
    }
}
// Маршрут /patch
app.post('/patch', upload.single('file'), (req, res) => {
    if (!req.file) {
        return res.status(400).send('No file uploaded');
    }
    if (!req.body.patches) {
        return res.status(400).send('No patches data uploaded');
    }

    const filePath = req.file.path;
    const patches = JSON.parse(req.body.patches);

    const patchData = {};
    for (const [key, value] of Object.entries(patches)) {
        patchData[key] = {
            type: docx.PatchType.PARAGRAPH,
            children: [new docx.TextRun(value)]
        };
    }

    docx.patchDocument(fs.readFileSync(filePath), {
        outputType: "nodebuffer",
        patches: patchData
    }).then(doc => {
        const patchedFilePath = 'uploads/patched-' + Date.now() + '.docx';
        fs.writeFileSync(patchedFilePath, doc);

        // Отправляем обработанный файл обратно на фронтенд
        res.download(patchedFilePath, () => {
            // Удаляем исходный и обработанный файлы после отправки
                fs.unlink(filePath, err => {
                    if (err) console.error('Error deleting original file:', err);
                });
                fs.unlink(patchedFilePath, err => {
                    if (err) console.error('Error deleting patched file:', err);
                });
        });
    }).catch(err => {
        console.error(err);
        res.status(500).send('Error processing the file');

        // Удаляем исходный файл в случае ошибки
        fs.unlink(filePath, err => {
            if (err) console.error('Error deleting original file:', err);
        });
    });
});

// Маршрут для загрузки файла
app.post('/upload', upload.single('file'), (req, res) => {
    if (!req.file) {
        return res.status(400).send('No file uploaded');
    }

    const fileExtension = path.extname(req.file.originalname).toLowerCase();
    if (fileExtension !== '.docx') {
        return res.status(400).send('Invalid file type. Only .docx files are allowed');
    }
    const filePath = req.file.path;

    extractPlaceholders(filePath)
        .then(placeholders => {
            console.log(placeholders);

            res.json(placeholders); // Отправляем данные обратно на фронтенд
        //     fs.unlink(filePath, err => {
        //         if (err) console.error('Error deleting uploaded file:', err);
        //     });
         })
        .catch(err => {
            console.error(err);
            res.status(500).send('Error processing the file'+ err.message);
            fs.unlink(filePath, err => {
                if (err) console.error('Error deleting uploaded file:', err);
            });
        });
});

app.post('/topdf', upload.single('file'), (req, res) => {
    if (!req.file) {
        return res.status(400).send('No file uploaded');
    }

    const filePath = req.file.path;

    extractPlaceholders(filePath)
  

            docxConverter(filePath, './output.pdf', function(err, result) {
                if (err) {
                    console.log(err);
                    res.status(500).send('Error converting to PDF');
                    return;
                }
                console.log('Conversion result: ' + result);

                // Проверка существования файла перед отправкой
                if (fs.existsSync('./output.pdf')) {
                    res.download('./output.pdf', 'converted.pdf', (err) => {
                        if (err) {
                            console.error('Error sending file:', err);
                        }

                        // Удаление PDF после отправки
                        fs.unlink('./output.pdf', err => {
                            if (err) console.error('Error deleting PDF file:', err);
                        });
                    });
                } else {
                    res.status(404).send('Converted file not found');
                }

                // Удаление исходного файла DOCX
                fs.unlink(filePath, err => {
                    if (err) console.error('Error deleting original file:', err);
                });
            });
});

app.listen(process.env.PORT || PORT,(error)=>{
    error ? console.log(error) : console.log(`listetning server on ${PORT}`);
});

app.use(morgan(':method :url :status :res[content-length] - :response-time ms'))